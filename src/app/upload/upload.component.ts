import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { DialogComponent } from './dialog/dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Store, select } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {cloneDeep} from 'lodash';
import { MatPaginator } from '@angular/material/paginator';
import { FileModel } from './model/interface';
import { FileEntityService } from './service/file-entity.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit, OnDestroy, AfterViewInit {

  displayedColumns: string[] = ['PK', 'name', 'type', 'uploadedAt', 'action'];
  dataSource: MatTableDataSource<FileModel> = new MatTableDataSource<FileModel>([]); // Initialize with an empty array

  private _unsubscribeAll: Subject<any> = new Subject<any>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  public files$: Observable<FileModel[]>;
  
  constructor(
    public dialog: MatDialog,
    public fileEntityService: FileEntityService,
    private store: Store
  ) { }

  ngOnInit() {
    this.fileEntityService.entities$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((files: FileModel[]) => {
        console.log({files})
        this.dataSource.data = files;
      });

  }

  openUploadDialog() {
    let dialogRef = this.dialog.open(DialogComponent, { width: '50%', height: '50%' });
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  downloadFile(file: FileModel) {
    this.fileEntityService.getByKey(`${file.PK}.${file.extention}`)
    .subscribe(
      (file: FileModel) => {
        window.open(file.url, '_blank');
      }
    )
  }

  delete(row: FileModel) {
    this.fileEntityService.delete(row)
    .subscribe(
      () => console.log("Delete completed"),
      err => console.log("Deleted failed", err)
    )
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
