export interface FileModel {
    PK?: string;
    SK?: string;
    extention?: string;
    GSI1?: string;
    name?: string;
    type?: string;
    updatedAt?: string;
    uploadedAt?: string;
    url?: string;
}