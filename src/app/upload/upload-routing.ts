import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UploadComponent } from './upload.component';
import { FileResolver } from './service/file.resolver';


const routes: Routes = [
    {
        path: '',
        component: UploadComponent,
        resolve: {
            files: FileResolver
        }
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UploadRoutingModule { }
