import {Injectable} from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {Observable} from 'rxjs';
import {filter, first, map, tap} from 'rxjs/operators';
import { FileEntityService } from './file-entity.service';


@Injectable()
export class FileResolver {
    
        constructor(
            private fileEntityService: FileEntityService,
        ) {
        }
    
        resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
            return this.fileEntityService.loaded$
            .pipe(
                tap(loaded => {
                    if (!loaded) {
                       this.fileEntityService.getAll();
                    }
                }),
                filter(loaded => !!loaded),
                first()
            );
        }
    }