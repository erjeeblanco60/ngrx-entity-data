import { Injectable } from '@angular/core';
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { FileModel } from '../model/interface';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class FileEntityService extends EntityCollectionServiceBase<FileModel> {
  constructor(
    serviceElementsFactory: EntityCollectionServiceElementsFactory,
    ) {
    super('File',  serviceElementsFactory);
  }


}
