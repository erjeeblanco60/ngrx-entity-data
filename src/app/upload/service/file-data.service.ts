import {Injectable} from '@angular/core';
import {DefaultDataService, HttpUrlGenerator} from '@ngrx/data';

import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map, mergeMap} from 'rxjs/operators';
import { FileModel } from '../model/interface';

@Injectable()
export class FileDataService extends DefaultDataService<FileModel> {

    constructor(http: HttpClient, httpUrlGenerator: HttpUrlGenerator) {
        super('File', http, httpUrlGenerator);
    }


    override add(file): Observable<FileModel> {
        const fileData = {
            name: file.name,
            type: file.type,
          }
          return this.http.post(`/api/upload-random`, fileData).pipe(
            mergeMap((result: any) => {
              return this.http.post(result.s3.url, this.parseFormData(result.s3.fields, file)).pipe(
                map(() => result.data)
              );
            })
          );
   }

   private parseFormData(fields: Map<string, string>, file: File): FormData {
    const formData = new FormData();
    Object.keys(fields).forEach((key) => {
      formData.append(key, fields[key]);
    });
    formData.append('file', file);
    return formData;
  }
}