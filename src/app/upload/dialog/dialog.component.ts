import { Component, OnInit, ViewChild } from '@angular/core';
import { EntityAction } from '@ngrx/data';


import { Observable, forkJoin } from 'rxjs';
import { MatDialogRef } from '@angular/material/dialog';
import { Store, select } from '@ngrx/store';

import { Actions, ofType } from '@ngrx/effects';
import { FileEntityService } from '../service/file-entity.service';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  @ViewChild('file', { static: false }) file;

  public files: Set<File> = new Set();
  public primaryButtonText = 'Upload';
  public isLoading$: Observable<boolean>;

  constructor(
    private actions$: Actions,
    public dialogRef: MatDialogRef<DialogComponent>,
    // private uploadService: UploadService,
    private fileEntityService: FileEntityService,
    ) { }

  ngOnInit() {
    this.isLoading$ = this.fileEntityService.loading$;
   }

  onFilesAdded() {
    const files = this.file.nativeElement.files;
    for (let key in files) {
      if (!isNaN(parseInt(key))) {
        this.files.add(files[key]);
      }
    }
  }

  addFiles() {
    this.file.nativeElement.click();
  }

  closeDialog() {
    this.files.forEach(file => {
      this.fileEntityService.add(file)
      .subscribe(() => {
        this.primaryButtonText = 'Finish';
        this.dialogRef.close();
      });
    });
  }
}
