import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadComponent } from './upload.component';
import { DialogComponent } from './dialog/dialog.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { UploadRoutingModule } from './upload-routing';
import { MaterialExampleModule } from '../../material.module';
import { DateFormatModule } from '../share/pipe/date-format.module';
import { EntityDataService, EntityDefinitionService, EntityMetadataMap } from '@ngrx/data';
import { FileDataService } from './service/file-data.service';
import { FileEntityService } from './service/file-entity.service';
import { FileResolver } from './service/file.resolver';
import { select } from '@ngrx/store';
import { FileModel } from './model/interface';

const entityMetadata: EntityMetadataMap = {
  File: {
      entityDispatcherOptions: {
          optimisticUpdate: true
      },
      selectId: (file: FileModel) => file.PK,
  },
};
@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    HttpClientModule,
    UploadRoutingModule,
    MaterialExampleModule,
    DateFormatModule,
  ],
  declarations: [UploadComponent, DialogComponent],
  bootstrap: [UploadComponent, DialogComponent],
  entryComponents: [DialogComponent], // Add the DialogComponent as entry component
  providers: [
    FileDataService,
    FileEntityService,
    HttpClientModule,
    FileResolver
  ]

})
export class UploadModule {
  constructor(
    private eds: EntityDefinitionService,
    private entityDataService: EntityDataService,
    private fileDataService: FileDataService
  ) {
    eds.registerMetadataMap(entityMetadata);
    entityDataService.registerService('File', fileDataService);
  }
}
