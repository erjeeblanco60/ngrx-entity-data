
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
const routes: Routes = [
    {path: '', pathMatch : 'full', redirectTo: 'home'},
    {
        path: 'home',
        loadChildren: () => import('./upload/upload.module').then(m => m.UploadModule)
    }
];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class ApproutingModule { }