import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment';

@Pipe({
  name: 'dateFormat',
})
export class DateFormatPipe implements PipeTransform {
  constructor() {}

  transform(
    value: any,
  ): any {
    return moment(value).format('DD/MM/YYYY');
  }
}
