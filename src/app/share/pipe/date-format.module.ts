import { DateFormatPipe } from './date-format.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [DateFormatPipe],
  imports: [CommonModule],
  exports: [DateFormatPipe],
  providers: [DateFormatPipe],
})
export class DateFormatModule {}
