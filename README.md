## Introduction


## NgRx Data

NgRx Data is an extension library for NgRx that simplifies the management of entities and their associated state in Angular applications. It provides a set of pre-defined actions, reducers, and selectors that streamline the process of handling CRUD operations and maintaining a consistent state for entity-based data. By utilizing NgRx Data, I have achieved the following benefits:

1. **Simplified Entity Management**: NgRx Data abstracts away the complexities of managing entities by automatically generating actions, reducers, and selectors based on entity metadata. This reduces the boilerplate code and allows for faster development.

2. **Normalized Data Structure**: NgRx Data adopts a normalized data structure where entities are stored in a normalized state tree. This ensures data consistency, eliminates redundancy, and facilitates efficient data retrieval.

3. **Efficient Change Tracking**: With NgRx Data, change detection is optimized by leveraging the `TrackBy` feature. This enables granular updates to the UI when entities are added, modified, or deleted, resulting in improved performance.

4. **Enhanced Data Persistence**: NgRx Data integrates seamlessly with backend APIs, providing built-in mechanisms for data persistence. It supports various backend communication patterns such as RESTful APIs, GraphQL, and WebSockets.

However, it's important to note that NgRx Data may have some limitations in certain scenarios. For instance, when dealing with complex data relationships or advanced querying capabilities, additional customization might be required. Nevertheless, for most standard CRUD operations and entity-based data management, NgRx Data offers significant advantages in terms of simplicity, consistency, and maintainability.

## Frontend Functionality

The purpose of the frontend application is to serve as the user interface for interacting with the backend APIs. It provides the following functionality:

1. **Table Listing**: The frontend displays a table that lists all the files retrieved from the backend. The table presents relevant information about each file, such as its name, type, and upload date.

2. **File Download**: For each file in the table, there is a button that allows users to download the file. When clicked, the frontend requests a signed URL from the backend with a 5-minute validity period. This signed URL grants temporary access to download the file securely.

3. **File Deletion**: The frontend includes a delete button associated with each file. Clicking this button triggers a delete request to the backend, removing the file from both the frontend UI and the backend storage.

4. **Search Functionality**: The frontend provides a search feature that allows users to filter the displayed files based on specific criteria. This functionality improves the user experience by enabling quick and efficient file discovery.

5. **Upload Functionality**: The frontend supports multiple file uploads of any file type. When a file is selected for upload, the frontend requests a signed post URL from the backend. This signed URL allows the frontend to securely upload the file to the backend storage.

By incorporating these features, the frontend application delivers a comprehensive user interface for managing files, enabling users to browse, download, delete, search, and upload files efficiently.

## Setup

1. Clone the repository and navigate to the frontend directory Angular application.

2. Install project dependencies by running `npm install`.

## Starting the Application with Proxy Config

1. Open a terminal or command prompt in the frontend's root directory.

2. Start the application with the proxy configuration by running `npm run start`.

3. The Angular development server will compile the application and serve it at `http://localhost:4200`.

4. Configure the proxy by creating a `proxy.json` file in the root directory and specify the target backend server and the API routes to be proxied.

5. Ensure that your backend server is running and accessible at the specified target URL in the proxy configuration.

That's it! You have successfully started your Angular application using the `npm run start` command with the proxy configuration. Happy coding!



---

## NgRx Data

NgRx/Data provides several services that simplify the management of entities in an NgRx-powered application. These services handle common CRUD (create, read, update, delete) operations and provide convenient methods for interacting with the entity collection in the NgRx Store. Here are the main services offered by NgRx/Data:

1. **EntityCollectionService**: This service acts as a facade for entity management. It provides methods for performing CRUD operations on entities, such as adding, updating, and deleting entities. It also offers additional functionality like querying entities based on specific criteria, sorting, and pagination.

2. **EntityDataService**: The EntityDataService is responsible for coordinating data operations with backend APIs. It provides a consistent interface for communicating with the server, including methods for fetching entity data, saving changes, and handling errors. It integrates with various HTTP communication patterns, such as RESTful APIs or GraphQL.


By utilizing these services, developers can leverage the power of NgRx/Data to handle entity management in a consistent and efficient manner. The services abstract away many of the complexities involved in managing entity state, data fetching, and synchronization with backend APIs. This results in cleaner and more maintainable code, improved performance, and reduced development effort.

It's important to note that while NgRx/Data provides powerful services for entity management, it may not be suitable for every application or use case. Developers should carefully consider their specific requirements and evaluate whether NgRx/Data aligns with their project's needs.

### EntityCollectionService

```typescript
import { Injectable } from '@angular/core';
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { FileModel } from '../model/interface';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class FileEntityService extends EntityCollectionServiceBase<FileModel> {
  constructor(
    serviceElementsFactory: EntityCollectionServiceElementsFactory,
  ) {
    super('File', serviceElementsFactory);
  }
}
```

- The `FileEntityService` is decorated with the `Injectable` decorator, indicating that it can be injected as a dependency in other Angular components.

- The service constructor takes one parameter:
  - `serviceElementsFactory`: This parameter is of type `EntityCollectionServiceElementsFactory` and is used to create the necessary elements for the service to interact with the entity collection.

- The `super` keyword is used to call the constructor of the base class `EntityCollectionServiceBase`. It takes two arguments:
  - `'File'`: This is the name of the entity collection that the service will manage. It is used to identify the collection in the NgRx/Data Store.
  - `serviceElementsFactory`: This argument is passed to the base class constructor, providing the necessary elements to interact with the entity collection.

By extending `EntityCollectionServiceBase<FileModel>`, the `FileEntityService` inherits various methods and functionality for managing the `FileModel` entities. This includes methods for adding, updating, and deleting entities, as well as querying and selecting entities from the collection.

The `FileEntityService` can leverage the HttpClient provided by Angular's `@angular/common/http` module to communicate with the backend API and perform CRUD operations on the `FileModel` entities.

Overall, the `FileEntityService` acts as a centralized service for managing the `FileModel` entity collection, abstracting away the complexities of entity state management and providing convenient methods for interacting with the collection in a NgRx/Data-compliant manner. It enables easy integration with the NgRx/Data library and simplifies the implementation of entity-related operations in an Angular application.

### EntityDataService

```typescript
import { Injectable } from '@angular/core';
import { DefaultDataService, HttpUrlGenerator } from '@ngrx/data';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FileModel } from '../model/interface';

@Injectable()
export class FileDataService extends DefaultDataService<FileModel> {
  constructor(http: HttpClient, httpUrlGenerator: HttpUrlGenerator) {
    super('File', http, httpUrlGenerator);
  }

  override getAll(): Observable<FileModel[]> {
    return this.http.get<FileModel[]>(`/api/files`);
  }
}
```
- The `FileDataService` is decorated with the `Injectable` decorator, indicating that it can be injected as a dependency in other Angular components.

- The service constructor takes two parameters:
  - `http`: An instance of the `HttpClient` from the `@angular/common/http` module. This is used for making HTTP requests to the backend API.
  - `httpUrlGenerator`: An instance of the `HttpUrlGenerator` provided by NgRx/Data. It is used to generate the URLs for the data operations based on the entity name.

- The `super` keyword is used to call the constructor of the base class `DefaultDataService`. It takes three arguments:
  - `'File'`: This is the name of the entity that the service will handle. It is used to identify the entity in the NgRx/Data Store.
  - `http`: The `HttpClient` instance passed to the service constructor.
  - `httpUrlGenerator`: The `HttpUrlGenerator` instance passed to the service constructor.

- The `override getAll()` method is implemented to retrieve all `FileModel` entities from the backend API. It returns an `Observable<FileModel[]>` which emits the array of `FileModel` objects retrieved from the `/api/files` endpoint.

Overall, the `FileDataService` acts as a data service specifically designed for the `FileModel` entity. It extends the `DefaultDataService` class to provide default data operations and can be customized to handle specific data retrieval, creation, update, and deletion operations for the entity. In this case, the `getAll()` method is overridden to retrieve all `FileModel` entities from the backend API using the `HttpClient`.

### Angular Resolve

In the provided code, there is an implementation of the Angular Resolve feature. The resolve feature allows you to fetch data or perform certain actions before the associated component is loaded, ensuring that the required data is available for the component to render.

### Route Configuration

```typescript
{
  path: '',
  component: UploadComponent,
  resolve: {
    data: FileListResolver
  }
}
```

In the route configuration, when the path is empty (`''`), the `UploadComponent` will be loaded, and a resolver called `FileListResolver` will be invoked. The resolver is assigned to the `data` property, indicating that it will resolve data required by the `UploadComponent`.

### Resolver Implementation

```typescript
resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
  return this.store.pipe(
    select(isFilesLoadedSelector),
    tap((isLoaded) => {
      if (!isLoaded) {
        this.store.dispatch(loadFiles());
      }
    }),
    first()
  );
}
```

The `FileListResolver` class implements the `Resolve` interface provided by Angular. It defines a method named `resolve` that accepts an `ActivatedRouteSnapshot` and a `RouterStateSnapshot`. This method returns an `Observable<boolean>`.

Within the resolver's `resolve` method, the `store` is accessed to retrieve the state of the files. The `select` function is used to select a specific slice of the state, in this case, the `isFilesLoadedSelector`, which presumably checks whether the files are already loaded.

If the `isLoaded` flag is `false`, indicating that the files are not loaded, the `loadFiles` action is dispatched to trigger the loading of files.

The resolver utilizes RxJS operators, such as `tap` and `first`, to perform side effects and ensure that the observable completes after the first emission.

Overall, the resolver ensures that the required data for the `UploadComponent` is loaded before the component is rendered. It helps in managing data dependencies and provides a seamless user experience by avoiding the display of incomplete or empty data.

